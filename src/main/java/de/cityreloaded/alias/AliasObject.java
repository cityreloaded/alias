package de.cityreloaded.alias;

import java.util.ArrayList;

import lombok.Data;

/**
 * Created by Lars on 22.10.2015.
 */
@Data
public class AliasObject {

  private String aliasCommand;

  private ArrayList<String> aliasMessages;

  private ArrayList<CommandObject> commandObjects;

  private boolean overwrite = true;

  private boolean wildcard = false;

  private String permission;

  private String noPermissionMessage;


  public AliasObject(String aliasCommand, ArrayList<String> aliasMessages, ArrayList<CommandObject> commandObjects) {
    this.setAliasCommand(aliasCommand);
    this.setAliasMessages(aliasMessages);
    this.setCommandObjects(commandObjects);
  }

  public AliasObject(String aliasCommand, ArrayList<String> aliasMessages, ArrayList<CommandObject> commandObjects, boolean overwrite, boolean wildcard, String permission) {
    this.setAliasCommand(aliasCommand);
    this.setAliasMessages(aliasMessages);
    this.setCommandObjects(commandObjects);
    this.setOverwrite(overwrite);
    this.setWildcard(wildcard);
    this.setPermission(permission);
  }

  public String getPermission() {
    return this.permission != null ? this.permission : "Alias." + this.getAliasCommand().replaceAll(" ", "_");
  }
}
