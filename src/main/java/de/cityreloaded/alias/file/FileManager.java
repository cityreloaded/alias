package de.cityreloaded.alias.file;

import de.cityreloaded.alias.AliasObject;
import de.cityreloaded.alias.CommandObject;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Lars on 22.10.2015.
 */
public class FileManager {
  private File aliasFolder;

  public FileManager() {
    this.aliasFolder = new File("./plugins/Alias/Alias/");
    this.aliasFolder.mkdirs();
  }

  public ArrayList<AliasObject> loadAliases() {
    ArrayList<AliasObject> aliasList = new ArrayList<>();
    File[] fileList = this.aliasFolder.listFiles((it) -> it.getName().endsWith(".yml"));

    if (fileList == null) {
      return new ArrayList<>();
    }

    for (File file : fileList) {
      try {
        InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(file), "UTF-8");
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(inputStreamReader);

        fileConfiguration.getKeys(false).forEach(command -> {
          ConfigurationSection configSection = fileConfiguration.getConfigurationSection(command);

          boolean overwrite = configSection.getBoolean("overwrite", true);
          boolean wildcard = configSection.getBoolean("wildcard", false);
          String permission = configSection.getString("permission", "Alias." + command);
          String noPermissionMessage = configSection.getString("noPermissionMessage", "Du hast keine Berechtigung für diesen Befehl!");

          ArrayList<String> aliasMessages = new ArrayList<>();
          ArrayList<CommandObject> commandObjects = new ArrayList<>();
          if (configSection.get("messages") != null) {
            configSection.getStringList("messages").stream()
                    .map(s -> ChatColor.translateAlternateColorCodes('&', s))
                    .forEach(aliasMessages::add);
          }

          ConfigurationSection commandSection = configSection.getConfigurationSection("commands");
          if (commandSection != null) {
            commandSection.getKeys(false).forEach(key -> {
              CommandObject commandObject = new CommandObject();
              commandObject.setRunAs(commandSection.getString(key + ".runas"));
              commandObject.setCommand(commandSection.getString(key + ".command"));
              commandObjects.add(commandObject);
            });
          }

          AliasObject aliasObject = new AliasObject(command, aliasMessages, commandObjects, overwrite, wildcard, permission);
          aliasObject.setNoPermissionMessage(noPermissionMessage);
          aliasList.add(aliasObject);
        });
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    return aliasList;
  }
}
