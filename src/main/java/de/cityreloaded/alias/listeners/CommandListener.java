package de.cityreloaded.alias.listeners;

import de.cityreloaded.alias.Alias;
import de.cityreloaded.alias.AliasObject;
import de.cityreloaded.alias.CommandObject;
import de.cityreloaded.alias.exceptions.InvalidArgumentException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Lars on 22.10.2015.
 */
public class CommandListener implements Listener {
  private final Pattern ARGUMENT_PATTERN = Pattern.compile("\\$(\\d)");
  private Alias alias;

  public CommandListener(Alias alias) {
    this.alias = alias;
  }

  @EventHandler(priority = EventPriority.HIGHEST)
  public void onCommand(PlayerCommandPreprocessEvent event) {
    if (event.getMessage().length() >= 2) {
      String command = event.getMessage().substring(1).toLowerCase().trim();
      AliasObject aliasObject = this.alias.getAliasManager().getByCommand(command);

      if (aliasObject != null) {
        this.executeAlias(aliasObject, event);
      }
    }
  }

  private void executeAlias(AliasObject aliasObject, PlayerCommandPreprocessEvent event) {
    Player player = event.getPlayer();
    String command = event.getMessage().substring(1).toLowerCase().trim();
    String[] arguments = command.split(" ");

    if (player.hasPermission(aliasObject.getPermission())/* || player.hasPermission("Alias.*")*/) {
      ArrayList<String> argumentList = new ArrayList<>(Arrays.asList(arguments.clone()));
      argumentList.remove(0);
      String[] argumentsWithoutRoot = argumentList.toArray(new String[argumentList.size()]);
      aliasObject.getAliasMessages().forEach(player::sendMessage);

      for (CommandObject commandObject : aliasObject.getCommandObjects()) {
        boolean isConsole = commandObject.getRunAs().equals("console");
        CommandSender commandSender = isConsole ? Bukkit.getConsoleSender() : player;

        try {
          String rewrite = this.replaceArguments(commandObject.getCommand(player), argumentsWithoutRoot);

          // Execute Aliases for rewrite manually as dispatchCommand does not trigger the PreProcessEvent
          AliasObject rewriteObject = this.alias.getAliasManager().getByCommand(rewrite);
          boolean hasCommand = rewriteObject != null;
          if (!isConsole && hasCommand && rewriteObject.isOverwrite()) {
            this.onCommand(new PlayerCommandPreprocessEvent(player, "/" + rewrite));
          } else if (!isConsole && hasCommand && !rewriteObject.isOverwrite()) {
            this.onCommand(new PlayerCommandPreprocessEvent(player, "/" + rewrite));
            Bukkit.dispatchCommand(commandSender, rewrite);
          } else if (!hasCommand) {
            Bukkit.dispatchCommand(commandSender, rewrite);
          }
        } catch (InvalidArgumentException e) {
          //TODO: Stop executing of aliases?
          player.sendMessage(this.alias.getCommandApi().getChatPrefix() + "Nicht genügend Argumente!");
        }
      }
    } else {
      player.sendMessage(ChatColor.RED + aliasObject.getNoPermissionMessage());
    }

    if (aliasObject.isOverwrite()) {
      event.setCancelled(true);
    }
  }

  private String replaceArguments(String command, String[] arguments) throws InvalidArgumentException {
    if (command.startsWith("/")) {
      command = command.substring(1);
    }

    Matcher matcher = this.ARGUMENT_PATTERN.matcher(command);

    while (matcher.find()) {
      int index = Integer.parseInt(matcher.group(1));

      int argumentIndex = index <= 0 ? 1 : index - 1;
      if (argumentIndex < arguments.length) {
        command = command.replaceAll("\\$" + (index), arguments[argumentIndex]);
      } else {
        throw new InvalidArgumentException("Not enough arguments given for $" + argumentIndex);
      }
    }
    command = command.replaceAll("\\$@", String.join(" ", arguments));

    return command;
  }
}
