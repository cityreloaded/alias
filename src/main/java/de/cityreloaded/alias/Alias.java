package de.cityreloaded.alias;

import de.cityreloaded.alias.commands.ReloadCommand;
import de.cityreloaded.alias.file.FileManager;
import de.cityreloaded.alias.listeners.CommandListener;
import de.paxii.bukkit.commandapi.CommandApi;

import org.bukkit.ChatColor;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;

/**
 * Created by Lars on 22.10.2015.
 */
public class Alias extends JavaPlugin {
  @Getter
  private static Alias instance;

  @Getter
  private FileManager fileManager;

  @Getter
  private AliasManager aliasManager;

  @Getter
  private CommandApi commandApi;

  @Override
  public void onEnable() {
    Alias.instance = this;
    this.fileManager = new FileManager();
    this.aliasManager = new AliasManager(this);
    this.commandApi = new CommandApi();

    this.loadRewrites();

    this.getServer().getPluginManager().registerEvents(new CommandListener(this), this);

    this.commandApi.setChatPrefix(ChatColor.GOLD + "[Alias] " + ChatColor.WHITE);
    this.commandApi.getCommandManager().addCommand(new ReloadCommand());

    this.getCommand("alias").setExecutor(this.commandApi.getExecutor());
  }

  public void loadRewrites() {
    this.aliasManager.loadAliases();
  }

  //TODO: Create different aliases for a command with and without arguments

  @Override
  public void onDisable() {
    PlayerCommandPreprocessEvent.getHandlerList().unregister(this);
  }

}

