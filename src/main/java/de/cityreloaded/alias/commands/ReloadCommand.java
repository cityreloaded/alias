package de.cityreloaded.alias.commands;

import de.cityreloaded.alias.Alias;
import de.paxii.bukkit.commandapi.command.AbstractCommand;

import org.bukkit.command.CommandSender;

/**
 * Created by Lars on 30.10.2016.
 */
public class ReloadCommand extends AbstractCommand {
  @Override
  public String getCommand() {
    return "reload";
  }

  @Override
  public String getHelp() {
    return "Reloads the aliases";
  }

  @Override
  public String getSyntax() {
    return this.getCommand();
  }

  @Override
  public String getPermission() {
    return "Alias.Admin.Reload";
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    Alias.getInstance().loadRewrites();
    commandSender.sendMessage(Alias.getInstance().getCommandApi().getChatPrefix() + "Reloaded Aliases.");

    return true;
  }
}
