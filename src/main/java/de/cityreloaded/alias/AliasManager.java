package de.cityreloaded.alias;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

import lombok.Getter;

/**
 * Created by Lars on 22.10.2015.
 */
public class AliasManager {
  private Alias alias;
  @Getter
  private HashMap<String, AliasObject> aliasObjects;

  public AliasManager(Alias alias) {
    this.aliasObjects = new HashMap<>();
    this.alias = alias;
    this.loadAliases();
  }

  public void loadAliases() {
    this.aliasObjects.clear();
    for (AliasObject aliasObject : this.alias.getFileManager().loadAliases()) {
      this.aliasObjects.put(aliasObject.getAliasCommand(), aliasObject);
    }
  }

  public AliasObject getByCommand(String command) {
    Set<String> keySet = this.alias.getAliasManager().getAliasObjects().keySet();
    String[] keys = keySet.toArray(new String[keySet.size()]);
    Arrays.sort(keys, (o1, o2) -> o1.length() > o2.length() ? 1 : -1);

    for (String key : keys) {
      AliasObject alias = this.alias.getAliasManager().getAliasObjects().get(key);

      if (alias.isWildcard()) {
        String pattern = alias.getAliasCommand() + "( .*)?";

        if (command.matches(pattern)) {
          return alias;
        }
      } else {
        if (command.equalsIgnoreCase(alias.getAliasCommand())) {
          return alias;
        }
      }
    }

    return null;
  }

  public boolean hasCommand(String command) {
    return this.getByCommand(command) != null;
  }
}
