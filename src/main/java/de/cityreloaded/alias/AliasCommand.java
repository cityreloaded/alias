package de.cityreloaded.alias;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Lars on 30.10.2016.
 */
public class AliasCommand {
  @Getter
  @Setter
  private String aliasCommand;

  @Getter
  @Setter
  private ArrayList<CommandObject> aliasCommands;

  @Getter
  @Setter
  private boolean overwrite = true;


  public AliasCommand(String aliasCommand, ArrayList<CommandObject> aliasCommands) {
    this.setAliasCommand(aliasCommand);
    this.setAliasCommands(aliasCommands);
  }

  public AliasCommand(String aliasCommand, ArrayList<CommandObject> aliasCommands, boolean overwrite) {
    this.setAliasCommand(aliasCommand);
    this.setAliasCommands(aliasCommands);
    this.setOverwrite(overwrite);
  }
}
