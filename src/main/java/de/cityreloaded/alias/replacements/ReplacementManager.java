package de.cityreloaded.alias.replacements;

import org.bukkit.ChatColor;

/**
 * Created by Lars on 22.10.2015.
 */
public class ReplacementManager {

  public static String replace(String toReplace) {
    return ChatColor.translateAlternateColorCodes('&', toReplace);
  }
}
