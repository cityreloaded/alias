package de.cityreloaded.alias;

import org.bukkit.entity.Player;

import lombok.Data;

/**
 * Created by Lars on 01.11.2016.
 */
@Data
public class CommandObject {
  private String runAs;
  private String command;

  public String getCommand(Player player) {
    return this.getCommand().replaceAll("%p", player.getName());
  }
}
