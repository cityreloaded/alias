package de.cityreloaded.alias.exceptions;

/**
 * Created by Lars on 01.11.2016.
 */
public class InvalidArgumentException extends Exception {
  public InvalidArgumentException(String message) {
    super(message);
  }
}
